Router.route('/', function () {
  this.render('browser');
});

Router.route('/table', function () {
  this.render('table');
});

Router.route('/list', function () {
  this.render('list');
});

Router.route('/scores', function () {
  this.render('ratings');
});

Router.route('/players', function () {
  this.render('players');
})

Router.route('/table/chgk', function () {
  this.render('chgk');
});

Router.route('/table/svoyak', function () {
  this.render('svoyak');
});

Router.route('/table/xamsa', function () {
  this.render('xamsa');
});

Router.route('/table/brainring', function () {
  this.render('brainring');
});

Router.route('/table/chgk/questions', function () {
  this.render('chgkQuestions');
});

Router.route('/table/chgk/game/24', function () {
  this.render('chgkGame24');
});

Router.route('table/chgk/game/36', function () {
  this.render('chgkGame36');
})
