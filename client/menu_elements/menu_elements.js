Template.list.helpers({
  list(){
    return Teams.find({});
  }
});

Template.list.events({
  'submit #addTeamForm'(e, i){
    var name = e.target.Name.value;
    var faculty = e.target.Faculty.value;
    e.target.Name.value = '';
    e.target.Faculty.value = '';
    Teams.insert({
      name,
      faculty,
      score : 0,
      inGame : false
    });
    return false;
  }
});

Template.ratings.helpers({
  list_faculty(){
    return Faculties.find({});
  },
  list_team(){
    return Teams.find({});
  }
});

Template.players.helpers({
  list(){
    return Players.find({});
  }
});

Template.players.events({
  'submit #addPlayer'(e, i){
    var name = e.target.name.value;
    var faculty = e.target.faculty.value;
    Players.insert({
      name,
      faculty,
      score : 0,
      inGame : false
    });
    return false;
  }
});
