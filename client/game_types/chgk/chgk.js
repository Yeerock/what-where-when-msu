Template.chgk.helpers({
  teams(){
    return Teams.find({});
  }
});

Template.chgk.events({
  'click #teams-proceed'(e, i){
    var teams = document.getElementsByName("team-check");
    for (var i = 0; i < teams.length; i++) {
      if (teams[i].checked) {

        var id = Teams.findOne({name : teams[i].value})._id;
        Teams.update({_id : id}, {
          $set : {inGame : true}
        });
      }
    }
  }
});

Template.chgkGame24.onCreated(function cg2() {
  this.result = new ReactiveVar(0);
})

Template.chgkGame24.helpers({
  teams(){
    return Teams.find({inGame : true});
  },
  questions(){
    var qArray = [];
    for (var i = 0; i < 24; i++) {
      qArray.push(i+1);
    }
    return qArray;
  }/*,

  result(){
    var teams = Teams.find({inGame : true}).fetch();
    teams.forEach(function (item, i, teams) {
      var i = 0;
      $(`tr[name="${item.name}"] input[type=checkbox]`).each(function() { if(this.checked) i++ });
    });
  }*/
});

Template.chgkGame24.events({
  'click #end-game'(e, i){
    while (Teams.findOne({inGame : true})) {
      var id = Teams.findOne({inGame : true})._id;
      Teams.update({_id : id}, {
        $set : {inGame : false}
      });
    }
  },
  'click .cb'(e, i){
    i.result.set(i.result.get()+1);
  }
});

Template.chgkGame36.onCreated(function cg3() {
  this.result = new ReactiveVar(0);
})

Template.chgkGame36.helpers({
  teams(){
    return Teams.find({inGame : true});
  },
  questions(){
    var qArray = [];
    for (var i = 0; i < 36; i++) {
      qArray.push(i+1);
    }
    return qArray;
  },
  result() {
    return Template.instance().result.get();
  }
});

Template.chgkGame36.events({
  'click #end-game'(e, i){
    while (Teams.findOne({inGame : true})) {
      var id = Teams.findOne({inGame : true})._id;
      Teams.update({_id : id}, {
        $set : {inGame : false}
      });
    }
  },
  'click .cb'(e, i){
    i.result.set(i.result.get()+1);
  },


});
